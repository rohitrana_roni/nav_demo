package com.example.hp.nav_demo;

import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
NavigationView navigationView;
DrawerLayout drawerLayout;
ImageButton imageButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            navigationView=findViewById(R.id.nav_view);
            drawerLayout=findViewById(R.id.Draw_lay);
            imageButton=findViewById(R.id.img_btn);
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(drawerLayout.isDrawerOpen(Gravity.START))
                    {
                        drawerLayout.closeDrawer(Gravity.START);
                    }
                    else{
                        drawerLayout.openDrawer(Gravity.START);

                    }
                }
            });





    }
}
